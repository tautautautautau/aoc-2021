f = open('inputs/input_1','r') # Change to correct file
L = f.readlines()

# Parse lines to correct format
for n in range(len(L)):
    L[n] = int(L[n])

# Part 1 solution
def part1():
    i = 0
    for n in range(len(L)-1):
        if L[n] < L[n+1]:
            i += 1
    return i

# Part 2 solution
def part2():
    i = 0
    for n in range(len(L)-3):
        A = L[n] + L[n+1] + L[n+2]
        B = L[n+1] + L[n+2] + L[n+3]
        if A < B:
            i += 1
    return i

# Print answers to parts 1 and 2
print(part1())
print(part2())