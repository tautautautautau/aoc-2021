f = open('inputs/input_3','r') # Change to correct file
L = f.readlines()

# Parse lines to correct format
for n in range(len(L)):
    L[n] = L[n].rstrip('\n')

# Part 1 solution
def part1():
    g = e = ''

    for p in range(len(L[0])):
        zeros = ones = 0
        for b in L:
            if int(b[p]) == 0:
                zeros += 1
            else:
                ones += 1
        if zeros > ones:
            g += '0'
            e += '1'
        else:
            g += '1'
            e += '0'
    return int(g,2)*int(e,2)

# Part 2 solution
def part2():
    return "Not implemented yet"

# Print answers to parts 1 and 2
print(part1())
print(part2())