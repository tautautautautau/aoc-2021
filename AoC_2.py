f = open('inputs/input_2','r') # Change to correct file
L = f.readlines()

# Parse lines to correct format

# Part 1 solution
def part1():
    h = d = 0
    for i in L:
        i = i.split(' ')
        a = i[0]
        x = int(i[1])
        if a == 'forward':
            h += x
        elif a == 'up':
            d -= x
        else:
            d += x
    return d*h

# Part 2 solution
def part2():
    h = d = a = 0
    for i in L:
        i = i.split(' ')
        A = i[0]
        x = int(i[1])
        if A == 'forward':
            h += x
            d += a*x
        elif A == 'up':
            a -= x
        else:
            a += x
    return d*h

# Print answers to parts 1 and 2
print(part1())
print(part2())