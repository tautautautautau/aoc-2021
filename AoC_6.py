f = open('inputs/input_6','r') # Change to correct file
L = f.readline().split(',')

# Parse lines to correct format
for i in range(len(L)):
    L[i] = int(L[i])

# Part 1 solution
def part1():
    l = L.copy()
    for day in range(80):
        for fish in range(len(l)):
            if l[fish] == 0:
                l.append(8)
                l[fish] = 6
            else:
                l[fish] -= 1
    return len(l)

# Part 2 solution
def part2():
    fishes = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(L)):
        fishes[L[i]] += 1
    for day in range(256):
        resetting = fishes[0]
        fishes[0] = fishes[1]
        fishes[1] = fishes[2]
        fishes[2] = fishes[3]
        fishes[3] = fishes[4]
        fishes[4] = fishes[5]
        fishes[5] = fishes[6]
        fishes[6] = fishes[7]
        fishes[7] = fishes[8]
        fishes[8] = resetting
        fishes[6] += resetting
    return sum(fishes)

# Print answers to parts 1 and 2
print(part1())
print(part2())