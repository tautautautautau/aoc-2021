f = open('inputs/input_9','r') # Change to correct file
L = f.readlines()

# Parse lines to correct format
G = list()
for line in L:
    G.append(list([int(i) for i in line.rstrip('\n')]))

# Part 1 solution
def part1():
    risk = 0
    for y in range(len(G)):
        for x in range(len(G[y])):
            try:
                top = G[y - 1][x]
            except:
                top = 9
            try:
                bottom = G[y + 1][x]
            except:
                bottom = 9
            try:
                left = G[y][x - 1]
            except:
                left = 9
            try:
                right = G[y][x + 1]
            except:
                right = 9
            if all(o > G[y][x] for o in [top, bottom, right, left]):
                risk += G[y][x] + 1
    return risk

# Part 2 solution
def part2():
    return "Not implemented yet"

# Print answers to parts 1 and 2
print(part1())
print(part2())