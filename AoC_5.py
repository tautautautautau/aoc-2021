f = open('inputs/input_5','r') # Change to correct file
L = f.readlines()

# Parse lines to correct format
G = list()
Lines = list()
for i in range(len(L)):
    G.append(L[i].strip('\n').split(' -> '))
for i in range(len(G)):
    for j in G[i]:
        Lines.append(j.split(','))

# Part 1 solution
def part1():
    for i in range(len(Lines)):
        for j in range(len(Lines[i])):
            Lines[i][j] = int(Lines[i][j])
    d = max(map(max, Lines))

    matrix = [([0]*d) for i in range(d)]

    for i in range(0, len(Lines), 2):
        p1 = Lines[i]
        p2 = Lines[i + 1]
        if p1[0] in p2:
            for j in range(p1[1], p2[1]+1):
                matrix[p1[0]][j] += 1
        elif p1[1] in p2:
            for j in range(p1[0], p2[0]+1):
                matrix[j][p1[1]] += 1

    ans = 0

    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] >= 2:
                ans += 1

    return ans #NOT DONE. GET'S WRONG ANSWER

# Part 2 solution
def part2():
    return "Not implemented yet"

# Print answers to parts 1 and 2
print(part1())
print(part2())