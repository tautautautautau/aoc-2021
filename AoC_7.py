f = open('inputs/input_7','r') # Change to correct file
L = f.readline().split(',')

# Parse lines to correct format
for i in range(len(L)):
    L[i] = int(L[i])

L_max = max(L)
L_min = min(L)

# Part 1 solution
def part1():
    fuel_amount = 9e9
    for x in range(L_min, L_max):
        d = 0
        for i in range(len(L)):
            d += abs(L[i] - x)
        if d < fuel_amount:
            fuel_amount = d
    return fuel_amount

# Part 2 solution
def part2():
    fuel_amount = 9e9
    for x in range(L_min, L_max):
        d = 0
        for i in range(len(L)):
            dist = abs(L[i] - x)
            d += int((dist * (dist + 1)) / 2)
        if d < fuel_amount:
            fuel_amount = d
    return fuel_amount

# Print answers to parts 1 and 2
print(part1())
print(part2())